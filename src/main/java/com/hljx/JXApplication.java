package com.hljx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = "com.hljx.mapper")
@SpringBootApplication(scanBasePackages = "com.hljx")
public class JXApplication {

    public static void main(String[] args) {
        SpringApplication.run(JXApplication.class, args);
    }

}
