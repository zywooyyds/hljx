package com.hljx.bo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author : gx
 * @since : 2023/5/11 15:12
 */
@Data
public class AccountingUnitsVO {
    @Schema(description ="核算单元主键")
    private Long id;

    @Schema(description = "院区id")
    private Long hospitalId;

    @Schema(description = "类别(字典id)")
    private Long dictItemId;

    @Schema(description ="核算单元名称")
    private String name;

    @Schema(description ="核算单元编码")
    private String code;

    @Schema(description ="类别")
    private String typeName;

    @Schema(description ="所属院区")
    private String hospitalAreaName;


}
