package com.hljx.bo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author : gx
 * @since : 2023/4/26 11:25
 */
@Data
public class SysDictVO {

    @Schema(description = "字典主键")
    private Long id;

    @Schema(description = "字典名称")
    private String name;

    @Schema(description = "字典编码")
    private String code;

    @Schema(description = "数据来源")
    private String source;

    @Schema(description = "是否启动 0禁止1启用")
    private String status;

    @Schema(description = "备注")
    private String description;

}
