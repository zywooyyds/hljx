package com.hljx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.vo.AssessmentCyclePullVO;
import com.hljx.config.Result;
import com.hljx.dto.AssessmentCycleDto;
import com.hljx.entity.AssessmentCycle;
import com.hljx.service.AssessmentCycleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 考核周期表 前端控制器
 * </p>
 *
 * @author lp
 * @since 2023-09-14
 */
@Tag(name = "考核周期")
@RestController
@RequestMapping("/assessmentCycle")
public class AssessmentCycleController {
    @Autowired
    private  AssessmentCycleService  assessmentCycleService;

    @Operation(summary = "新增考核周期")
    @PostMapping("/saveAssessmentCycle")
    public Result saveAssessmentCycle(@RequestBody AssessmentCycle  assessmentCycle) {
        boolean b = assessmentCycleService.saveAssessmentCycle(assessmentCycle);
            return Result.success(b);
    }

    @Operation(summary = "查询考核周期")
    @PostMapping("/listAll")
    public Result<IPage<AssessmentCycle>> listAll(@RequestBody AssessmentCycleDto assessmentCycleDto) {
        IPage<AssessmentCycle> assessmentCycleIPage = assessmentCycleService.listAll(assessmentCycleDto);
        return Result.success(assessmentCycleIPage);
    }
    @Operation(summary = "删除考核周期")
    @GetMapping("/deleteAssessmentCycleById")
    public Result deleteAssessmentCycleById(@RequestParam long  id) {
        boolean b = assessmentCycleService.deleteAssessmentCycleById(id);
        return Result.success(b );
    }

    @Operation(summary = "获取归档信息")
    @GetMapping("/getAssessmentCycleById")
    public Result<AssessmentCycle> getAssessmentCycleById(@RequestParam long  id) {
        AssessmentCycle  assessmentCycleDto  = assessmentCycleService.getById(id);
        return Result.success(assessmentCycleDto);
    }




    @Operation(summary = "工作量指标管理下拉框")
    @GetMapping("/getAssessmentCyclePull")
    public Result getAssessmentCyclePull(){
        List<AssessmentCyclePullVO> assessmentCyclePull = assessmentCycleService.getAssessmentCyclePull();
        return Result.success(assessmentCyclePull);
    }


}
