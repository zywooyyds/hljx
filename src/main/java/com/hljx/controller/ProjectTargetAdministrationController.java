package com.hljx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.vo.ProjectTargetAdministrationVO;
import com.hljx.config.Result;
import com.hljx.dto.ProjectTargetAdministrationDTO;
import com.hljx.entity.ProjectTargetAdministration;
import com.hljx.service.ProjectTargetAdministrationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Tag(name = "统计项目管理")
@RestController
@RequestMapping("/targetAdministration")
public class ProjectTargetAdministrationController {

    @Autowired
    private ProjectTargetAdministrationService projectTargetAdministrationService;


    @PostMapping("/listByCodeOrName")
    @Operation(summary = "分页模糊查询项目")
    public Result listByCodeOrName(@RequestBody ProjectTargetAdministrationDTO projectTargetAdministrationDTO){
        IPage<ProjectTargetAdministrationVO> projectTargetAdministrationVOIPage = projectTargetAdministrationService.listByCodeOrName(projectTargetAdministrationDTO);
        return Result.success(projectTargetAdministrationVOIPage);

    }

    @PostMapping("insertProject")
    @Operation(summary = "添加项目")
    public Result insertProject(@RequestBody ProjectTargetAdministration projectTargetAdministration){
        int insert = projectTargetAdministrationService.insertProject(projectTargetAdministration);
        return insert != 0 ? Result.success() : Result.failed();
    }

    @PostMapping("/deleteProject")
    @Operation(summary = "删除项目")
    public Result deleteProject(Long id){
        int i = projectTargetAdministrationService.deleteProject(id);
        return i != 0 ? Result.success() : Result.failed();
    }

}
