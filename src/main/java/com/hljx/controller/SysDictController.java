package com.hljx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.bo.vo.SysDictVO;
import com.hljx.config.Result;
import com.hljx.dto.SysDictDTO;
import com.hljx.dto.SysDictPageDTO;
import com.hljx.service.SysDictService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 字典 前端控制器
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@RestController
@RequestMapping("/sysDict")
@Tag(name = "字典管理")
public class SysDictController {

    @Autowired
    SysDictService dictService;

    @Operation(summary = "字典管理-新增或编辑")
    @PostMapping("/saveOrUpdateDict")
    public Result<Boolean> saveOrUpdateDict(@RequestBody @Valid SysDictDTO sysDictDTO) {
        return Result.success(dictService.saveOrUpdateDict(sysDictDTO));
    }

    @Operation(summary = "字典管理-删除")
    @GetMapping("/deleteDict")
    public Result<Boolean> deleteDict(@RequestParam("id") Long id) {
        return Result.success(dictService.deleteDict(id));
    }

    @Operation(summary = "字典管理-分页查询")
    @PostMapping("/listSysDictByPage")
    public Result<IPage<SysDictVO>> listSysDictByPage(@RequestBody SysDictPageDTO sysDictPageDTO) {
        return Result.success(dictService.listSysDictByPage(sysDictPageDTO));
    }


}
