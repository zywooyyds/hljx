package com.hljx.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 字典项 前端控制器
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@RestController
@RequestMapping("/sysDictItem")
@Tag(name = "字典数据项管理")
public class SysDictItemController {

}
