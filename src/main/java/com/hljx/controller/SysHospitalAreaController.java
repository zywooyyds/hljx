package com.hljx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.bo.vo.HospitalAreasVO;
import com.hljx.config.Result;
import com.hljx.dto.HospitalAreaDTO;
import com.hljx.dto.HospitalAreaPageDTO;
import com.hljx.service.SysHospitalAreaService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 院区管理 前端控制器
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@RestController
@RequestMapping("/sysHospitalArea")
@Tag(name = "院区管理")
public class SysHospitalAreaController {

    @Autowired
    private SysHospitalAreaService service;

    @Operation(summary = "保存或编辑院区")
    @PostMapping("/saveOrUpdateHospitalArea")
    public Result<Boolean> saveOrUpdateHospitalArea(@RequestBody @Valid HospitalAreaDTO hospitalAreaDTO) {
        return Result.success(service.saveOrUpdateHospitalArea(hospitalAreaDTO));
    }

    @Operation(summary = "删除院区")
    @GetMapping("/deleteHospitalArea")
    public Result<Boolean> deleteHospitalArea(@RequestParam("id") Long id) {
        return Result.success(service.deleteHospitalArea(id));
    }

    @Operation(summary = "分页查询院区")
    @PostMapping("/listHospitalAreaByPage")
    public Result<IPage<HospitalAreasVO>> listHospitalAreaByPage(@RequestBody HospitalAreaPageDTO hospitalAreaPageDTO) {
        return Result.success(service.listHospitalAreaByPage(hospitalAreaPageDTO));
    }

}
