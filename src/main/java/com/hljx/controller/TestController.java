package com.hljx.controller;

import com.hljx.interfaces.LifeClient;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qtx
 * @since 2023/8/28
 */
@Tag(name = "测试controller")
@RestController
@RequestMapping("/test")
public class TestController {

    private final LifeClient lifeClient;

    public TestController(LifeClient lifeClient) {
        this.lifeClient = lifeClient;
    }

    @Operation(summary = "get测试接口testPerson")
    @GetMapping("/testPerson")
    public void testPerson() {
        lifeClient.test("29002c000851383131333635");
    }

}
