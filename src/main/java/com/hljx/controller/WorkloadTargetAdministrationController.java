package com.hljx.controller;

import com.hljx.config.Result;
import com.hljx.dto.WorkloadTargetAdministrationCopyDTO;
import com.hljx.dto.WorkloadTargetAdministrationDTO;
import com.hljx.dto.WorkloadTargetAdministrationInsertDTO;
import com.hljx.service.WorkloadTargetAdministrationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 工作量指标管理主表 前端控制器
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Tag(name = "工作量指标管理")
@RestController
@RequestMapping("/workloadTargetAdministration")
public class WorkloadTargetAdministrationController {

    @Autowired
    private WorkloadTargetAdministrationService workloadTargetAdministrationService;


    @PostMapping("/insertWorkloadTargetAdministration")
    @Operation(summary = "新增工作量指标管理")
    public Result insertWorkloadTargetAdministration(@RequestBody WorkloadTargetAdministrationInsertDTO insertDTO){
        int i = workloadTargetAdministrationService.insertWorkloadTargetAdministration(insertDTO);
        return i != 0 ? Result.success() : Result.failed();
    }

    @PostMapping("/updateWorkloadTargetAdministration")
    @Operation(summary = "编辑工作量指标管理")
    public Result updateWorkloadTargetAdministration(@RequestBody WorkloadTargetAdministrationInsertDTO updateDTO){
        int i = workloadTargetAdministrationService.updateWorkloadTargetAdministration(updateDTO);
        return i != 0 ? Result.success() : Result.failed();
    }


    @PostMapping("/inherit")
    @Operation(summary = "继承")
    public Result inherit(@RequestBody WorkloadTargetAdministrationCopyDTO copyDTO){
        int i = workloadTargetAdministrationService.inheritWorkloadTargetAdministration(copyDTO);
        return i != 0 ? Result.success() : Result.failed();
    }


    @PostMapping("/deleteWorkloadAdministration")
    @Operation(summary = "删除")
    public Result deleteWorkloadAdministration(@RequestBody WorkloadTargetAdministrationDTO workloadTargetAdministrationDTO){
        int i = workloadTargetAdministrationService.deleteWorkloadAdministration(workloadTargetAdministrationDTO);
        return i != 0 ? Result.success() : Result.failed();
    }

}
