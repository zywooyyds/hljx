package com.hljx.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author : gx
 * @since : 2023/5/11 14:52
 */
@Data
public class AccountingUnitDTO {
    @Schema(description = "核算单元主键")
    private Long id;

    @Schema(description = "院区id")
    private Long hospitalId;

    @Schema(description = "类别(字典id)")
    private Long dictItemId;

    @Schema(description = "核算单元编码")
    private String code;

    @Schema(description = "核算单元名称")
    private String name;
}
