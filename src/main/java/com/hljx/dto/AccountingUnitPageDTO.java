package com.hljx.dto;

import com.hljx.config.PageDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author : gx
 * @since : 2023/5/11 15:36
 */
@Data
public class AccountingUnitPageDTO extends PageDTO {

    @Schema(description = "综合搜索 编码或名称")
    private String nameOrCode;

    @Schema(description = "院区id")
    private Long hospitalId;

    @Schema(description = "类别(字典id)")
    private Long dictItemId;
}
