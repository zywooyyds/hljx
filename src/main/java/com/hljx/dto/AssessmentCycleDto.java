package com.hljx.dto;

import com.hljx.config.PageDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @BelongsProject: hljx
 * @BelongsPackage: com.hljx.dto
 * @Author: lp
 * @CreateTime: 2023-09-18  13:54
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class AssessmentCycleDto extends PageDTO {

    @Schema(description = "关键字")
    private String  keyWord;
}
