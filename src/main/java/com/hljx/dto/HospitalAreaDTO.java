package com.hljx.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;



/**
 * @author : gx
 * @since : 2023/5/10 17:10
 */
@Data
public class HospitalAreaDTO {

    @Schema(description = "院区主键")
    private Long id;

    @Schema(description = "编码")
    @NotBlank(message = "编码不能为空")
    private String code;

    @Schema(description = "名称")
    @NotBlank(message = "院区名称不能为空")
    private String name;

    @Schema(description = "备注")
    private String remark;
}
