package com.hljx.dto;

import com.hljx.config.PageDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author : gx
 * @since : 2023/5/10 17:37
 */
@Data
public class HospitalAreaPageDTO extends PageDTO {
    @Schema(description = "综合搜索 编码或名称")
    private String nameOrCode;
}
