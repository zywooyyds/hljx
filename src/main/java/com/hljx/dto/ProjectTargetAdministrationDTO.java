package com.hljx.dto;

import com.hljx.config.PageDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @Author: 何现成
 * @Date: 2023/9/18 14:14
 */
@Data
public class ProjectTargetAdministrationDTO extends PageDTO {

    @Schema(description = "综合搜索")
    private String nameOrNumber;

    @Schema(description = "来源")
    private Long source;
}
