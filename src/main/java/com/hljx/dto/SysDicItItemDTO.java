package com.hljx.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author : gx
 * @since : 2023/4/26 18:51
 */

@Data
public class SysDicItItemDTO {

    @Schema(description = "主键id")
    private Long id;

    @NotNull(message = "父级id不能为空")
    @Schema(description = "父级id")
    private Long dictId;

    @NotBlank(message = "字典标签不能为空")
    @Schema(description = "字典标签")
    private String name;

    @NotBlank(message = "字典code不能为空")
    @Schema(description = "字典code")
    private String code;

    @Schema(description = "字典排序")
    private String sort;

    @Schema(description = "是否启动 0禁止1启用")
    private Integer status;

    @Schema(description = "备注")
    private String remark;
}
