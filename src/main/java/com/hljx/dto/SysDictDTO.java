package com.hljx.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;


/**
 * @author : gx
 * @since : 2023/4/25 10:17
 */
@Data
public class SysDictDTO{

    @Schema(description = "主键id")
    private Long id;

    @NotBlank(message = "字典名称不能为空")
    @Schema(description = "字典名称")
    private String name;

    @NotBlank(message = "字典编号不能为空")
    @Schema(description = "字典编码")
    private String code;

    @NotBlank(message = "数据来源不能为空")
    @Schema(description = "数据来源")
    private String source;

    @Schema(description = "是否启动 0禁止1启用")
    private Integer status;

    @Schema(description = "备注")
    private String description;

}
