package com.hljx.dto;

import com.hljx.config.PageDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author : gx
 * @since : 2023/4/26 11:30
 */
@Data
public class SysDictPageDTO extends PageDTO {

    @Schema(description = "父id")
    private Long dictId;

    @Schema(description = "综合搜索 字典名称或编号")
    private String nameOrCode;

    @Schema(description = "是否启动 0禁止1启用")
    private Integer status;
}
