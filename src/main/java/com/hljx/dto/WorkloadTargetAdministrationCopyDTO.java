package com.hljx.dto;

import com.hljx.config.PageDTO;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @Author: 何现成
 * @Date: 2023/9/19 11:29
 */
public class WorkloadTargetAdministrationCopyDTO{



    @Schema(description = "被继承周期")
    private String beInheritedPeriod;

    @Schema(description = "继承")
    private String inheritPeriod;

}
