package com.hljx.dto;

import com.hljx.config.PageDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @Author: 何现成
 * @Date: 2023/9/19 11:29
 */
@Data
public class WorkloadTargetAdministrationDTO extends PageDTO {



    @Schema(description = "综合搜索")
    private String codeOrName;

    @Schema(description = "周期")
    private String period;

    @Schema(description = "主键id")
    private Long id;

}
