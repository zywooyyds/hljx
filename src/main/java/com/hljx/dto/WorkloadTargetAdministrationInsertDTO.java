package com.hljx.dto;

import com.hljx.entity.WorkloadTargetDept;
import com.hljx.entity.WorkloadTargetProject;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @Author: 何现成
 * @Date: 2023/9/18 17:14
 */
@Data
public class WorkloadTargetAdministrationInsertDTO {

    /**
     * 工作量指标编码
     */
    @Schema(description = "工作量指标编码")
    private String workloadCode;

    /**
     * 工作量指标名称
     */
    @Schema(description = "工作量指标名称")
    private String workloadName;

    @Schema(description = "工作量指标id")
    private Long id;

    @Schema(description = "周期")
    private String period;

    @Schema(description = "科室")
    private List<WorkloadTargetDept> dept;

    @Schema(description = "项目")
    private List<WorkloadTargetProject> project;


}
