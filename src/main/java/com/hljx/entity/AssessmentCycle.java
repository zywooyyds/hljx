package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hljx.config.BaseEntity;
import java.time.LocalDate;
import java.time.LocalDateTime;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 考核周期表
 * </p>
 *
 * @author lp
 * @since 2023-09-14
 */
@Getter
@Setter
@TableName("assessment_cycle")
public class AssessmentCycle extends BaseEntity {

    /**
     * 年
     */
    @TableField("year")
    @Schema(description = "年")
    private Integer year;

    /**
     * 次数
     */
    @TableField("times")
    @Schema(description = "次数")
    private Integer times;

    /**
     * 考核周期名称
     */
    @TableField("assessment_cycle_name")
    @Schema(description = "考核周期名称")
    private String assessmentCycleName;

    /**
     * 开始时间
     */
    @TableField("start_date")
    @Schema(description = "开始时间")
    private LocalDate startDate;

    @TableField("is_file")
    @Schema(description = "0是未归档1是已归档")
    private Integer isFile;

    /**
     * 结束时间
     */
    @TableField("end_date")
    @Schema(description = "结束时间")
    private LocalDate endDate;


}
