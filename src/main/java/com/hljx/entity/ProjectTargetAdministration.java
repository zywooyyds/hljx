package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hljx.config.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Getter
@Setter
@TableName("target_administration")
public class ProjectTargetAdministration extends BaseEntity {

    /**
     * 统计项目编码
     */
    @TableField("project_number")
    @Schema(description = "项目编码")
    private String projectCode;

    /**
     * 统计项目名称
     */
    @TableField("project_name")
    @Schema(description = "项目名称")
    private String projectName;

    /**
     * 来源
     */
    @TableField("source")
    @Schema(description = "来源")
    private String source;

    /**
     * 备注
     */
    @TableField("remark")
    @Schema(description = "备注")
    private String remark;


}
