package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hljx.config.BaseEntity;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 核算单元管理
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Getter
@Setter
@TableName("sys_accounting_unit")
public class SysAccountingUnit extends BaseEntity {

    /**
     * 院区id
     */
    @TableField("hospital_id")
    private Long hospitalId;

    /**
     * 字典数据项id(类别)
     */
    @TableField("dict_item_id")
    private Long dictItemId;

    /**
     * 核算单元名称
     */
    @TableField("name")
    private String name;

    /**
     * 核算单元编码
     */
    @TableField("code")
    private String code;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;
}
