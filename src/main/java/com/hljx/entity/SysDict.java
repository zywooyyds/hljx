package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hljx.config.BaseEntity;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 字典
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Getter
@Setter
@TableName("sys_dict")
public class SysDict extends BaseEntity {

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 编码
     */
    @TableField("code")
    private String code;

    /**
     * 启用状态：0->禁用 1->启用
     */
    @TableField("status")
    private Integer status;

    /**
     * 数据来源(值只能是this_system/his/nursing_management_system/supply_room_system/hand_numbness_system)
     */
    @TableField("source")
    private String source;

    /**
     * 备注
     */
    @TableField("description")
    private String description;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;
}
