package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hljx.config.BaseEntity;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 字典项
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Getter
@Setter
@TableName("sys_dict_item")
public class SysDictItem extends BaseEntity {

    /**
     * 字典父id
     */
    @TableField("dict_id")
    private Long dictId;

    /**
     * 字典标签
     */
    @TableField("name")
    private String name;

    /**
     * 字典code
     */
    @TableField("code")
    private String code;

    /**
     * 字典排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 启用状态：0->禁用 1->启用
     */
    @TableField("status")
    private Integer status;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    @TableField("create_time")
    private LocalDateTime createTime;

    @TableField("update_time")
    private LocalDateTime updateTime;
}
