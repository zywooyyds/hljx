package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hljx.config.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 工作量指标管理主表
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Getter
@Setter
public class WorkloadTargetAdministration extends BaseEntity {

    /**
     * 工作量指标编码
     */
    @TableField("workload_code")
    private String workloadCode;

    /**
     * 工作量指标名称
     */
    @TableField("workload_name")
    private String workloadName;

    /**
     * 0是已归档/1是未归档
     */
    @TableField("is_file")
    private Boolean isFile;

}
