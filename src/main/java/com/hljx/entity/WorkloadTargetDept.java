package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hljx.config.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 工作量指标管理科室信息表
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Getter
@Setter
public class WorkloadTargetDept extends BaseEntity {

    /**
     * 科室信息
     */
    @TableField("dept_code")
    private String deptCode;

    /**
     * 工作量指标主键id
     */
    @TableField("workload_target_administration_id")
    private Long workloadTargetAdministrationId;


}
