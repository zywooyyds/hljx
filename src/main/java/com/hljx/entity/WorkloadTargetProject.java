package com.hljx.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hljx.config.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 工作量指标管理项目信息表
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Getter
@Setter
public class WorkloadTargetProject extends BaseEntity {

    /**
     * 工作量指标管理主键id
     */
    @TableField("workload_target_administration_id")
    private Long workloadTargetAdministrationId;

    /**
     * 统计项目管理主键id
     */
    @TableField("target_administration_id")
    private Long targetAdministrationId;

    /**
     * 点数
     */
    @TableField("count")
    private Object count;

    /**
     * 单位
     */
    @TableField("unit")
    private Long unit;

}
