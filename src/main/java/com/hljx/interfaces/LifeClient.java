package com.hljx.interfaces;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

/**
 *
 * @author qtx
 * @since 2023/8/21
 */
@HttpExchange(value = "/results")
public interface LifeClient {

    @GetExchange("/test")
    void test(@RequestParam String userId);
}
