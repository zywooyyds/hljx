package com.hljx.mapper;

import com.hljx.vo.AssessmentCyclePullVO;
import com.hljx.entity.AssessmentCycle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 考核周期表 Mapper 接口
 * </p>
 *
 * @author lp
 * @since 2023-09-14
 */
@Mapper
public interface AssessmentCycleMapper extends BaseMapper<AssessmentCycle> {

    int     searchAllByStartDateAndEndDateInt(AssessmentCycle  assessmentCycle);

    void    copyTable(String keyWord );



    /*
     * 工作量指标管理下拉框
     *
     */
    List<AssessmentCyclePullVO> getAssessmentCyclePull();


}
