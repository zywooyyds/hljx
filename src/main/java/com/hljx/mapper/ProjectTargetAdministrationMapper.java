package com.hljx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.vo.ProjectTargetAdministrationVO;
import com.hljx.dto.ProjectTargetAdministrationDTO;
import com.hljx.entity.ProjectTargetAdministration;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Mapper
public interface ProjectTargetAdministrationMapper extends BaseMapper<ProjectTargetAdministration> {

    /**
     * @param
     * @author hxc
     * @date 2023/9/18 9:32
     * @version 1.0
     * @see
     */
    IPage<ProjectTargetAdministrationVO> listByCodeOrName(@Param("page") Page<ProjectTargetAdministrationVO> page, @Param("dto") ProjectTargetAdministrationDTO projectTargetAdministrationDTO);


    /**
     * @param
     * @author hxc
     * @date 2023/9/18 16:41
     * @version 1.0
     * @see "判重"
     */
    int distinct(@Param("id") Long id,@Param("projectCode") String projectNumber);

    /**
     * @param
     * @author hxc
     * @date 2023/9/18 16:51
     * @version 1.0
     * @see  "逻辑删除"
     */
    int updateByidDelete(Long id);
}
