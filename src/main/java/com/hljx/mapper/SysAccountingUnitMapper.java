package com.hljx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.bo.vo.AccountingUnitsVO;
import com.hljx.dto.AccountingUnitPageDTO;
import com.hljx.entity.SysAccountingUnit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 核算单元管理 Mapper 接口
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Mapper
public interface SysAccountingUnitMapper extends BaseMapper<SysAccountingUnit> {
    /**
     * 分页查询核算单元
     * @param page
     * @param dto
     * @return
     */
    IPage<AccountingUnitsVO> listAccountingUnitByPage(@Param("page") Page<AccountingUnitsVO> page, @Param("dto") AccountingUnitPageDTO dto);


}
