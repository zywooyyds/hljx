package com.hljx.mapper;

import com.hljx.entity.SysDictItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 字典项 Mapper 接口
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
