package com.hljx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.bo.vo.SysDictVO;
import com.hljx.dto.SysDictPageDTO;
import com.hljx.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 字典 Mapper 接口
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {
    /**
     * 分页查询字典
     * @param page
     * @param dto
     * @return
     */
    IPage<SysDictVO> listSysDictByPage(@Param("page") Page<SysDictVO> page,
                                       @Param("dto") SysDictPageDTO dto);

}
