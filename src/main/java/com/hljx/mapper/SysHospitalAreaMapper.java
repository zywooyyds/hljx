package com.hljx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.bo.vo.HospitalAreasVO;
import com.hljx.dto.HospitalAreaPageDTO;
import com.hljx.entity.SysHospitalArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 院区管理 Mapper 接口
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Mapper
public interface SysHospitalAreaMapper extends BaseMapper<SysHospitalArea> {
    /**
     * 分页查询字典
     * @param page
     * @param dto
     * @return
     */
    IPage<HospitalAreasVO> listHospitalAreaByPage(@Param("page") Page<HospitalAreasVO> page,
                                                  @Param("dto") HospitalAreaPageDTO dto);

}
