package com.hljx.mapper;

import com.hljx.dto.WorkloadTargetAdministrationCopyDTO;
import com.hljx.dto.WorkloadTargetAdministrationInsertDTO;
import com.hljx.entity.WorkloadTargetAdministration;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 工作量指标管理主表 Mapper 接口
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Mapper
public interface WorkloadTargetAdministrationMapper extends BaseMapper<WorkloadTargetAdministration> {


    /*
      新增工作量指标管理
     */
    int insertWorkloadTarget(@Param("period") String period,@Param("insertDTO") WorkloadTargetAdministrationInsertDTO insertDTO);

    /*
     * 工作量指标管理主表判重
     * */
    int distinct(String workloadCode);

    /*
     * 根据code查询主键id
     * */
    Long byCodeGetId(String period,String code);

    /*
     * 继承
     * */
    int inheritWorkloadTargetAdministration(@Param("copyDTO")WorkloadTargetAdministrationCopyDTO copyDTO);

    /*
     * 删除
     * */
    int deleteByWorkloadTargetAdministrationId(String period,long id);
}
