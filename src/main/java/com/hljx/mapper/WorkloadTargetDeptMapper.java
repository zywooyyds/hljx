package com.hljx.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.dto.WorkloadTargetAdministrationCopyDTO;
import com.hljx.vo.WorkloadTargetAdministrationDeptProjectVO;
import com.hljx.dto.WorkloadTargetAdministrationDTO;
import com.hljx.entity.WorkloadTargetDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 工作量指标管理科室信息表 Mapper 接口
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Mapper
public interface WorkloadTargetDeptMapper extends BaseMapper<WorkloadTargetDept> {


    IPage<WorkloadTargetAdministrationDeptProjectVO> listByPage(@Param("page") Page<WorkloadTargetAdministrationDeptProjectVO> page, @Param("dto")WorkloadTargetAdministrationDTO dto);

    /*
     * 继承，复制表
     */
    int inheritWorkloadTargetDept(@Param("copyDTO") WorkloadTargetAdministrationCopyDTO copyDTO);

    /*
     * 根据工作量指标管理主键id，删除科室信息
     */
    int ByWorkloadTargetAdministrationIdDeleteDept(long id,String period);

    int deleteByWorkloadTargetDept(String period,long id);

}
