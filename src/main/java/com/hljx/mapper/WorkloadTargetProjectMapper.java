package com.hljx.mapper;

import com.hljx.dto.WorkloadTargetAdministrationCopyDTO;
import com.hljx.entity.WorkloadTargetProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 工作量指标管理项目信息表 Mapper 接口
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Mapper
public interface WorkloadTargetProjectMapper extends BaseMapper<WorkloadTargetProject> {

    /*
     * 继承，复制表格
     */
    int inheritWorkloadTargetProject(@Param("copyDTO") WorkloadTargetAdministrationCopyDTO copyDTO);


    /*
     * 根据工作量指标管理id删除项目信息
     */
    int ByWorkloadTargetAdministrationIdDeleteProject(long id,String period);

    /*
     * 逻辑删除
     */
    int deleteByWorkloadTargetProject(String period,long id);

}
