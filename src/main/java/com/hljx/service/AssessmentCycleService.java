package com.hljx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.vo.AssessmentCyclePullVO;
import com.hljx.dto.AssessmentCycleDto;
import com.hljx.entity.AssessmentCycle;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 考核周期表 服务类
 * </p>
 *
 * @author lp
 * @since 2023-09-14
 */
public interface AssessmentCycleService extends IService<AssessmentCycle> {

    boolean  saveAssessmentCycle(AssessmentCycle  assessmentCycle);
    IPage<AssessmentCycle> listAll(AssessmentCycleDto assessmentCycleDto);
    boolean    deleteAssessmentCycleById(long  id);


    List<AssessmentCyclePullVO> getAssessmentCyclePull();

}
