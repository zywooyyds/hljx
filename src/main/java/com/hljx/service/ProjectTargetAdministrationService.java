package com.hljx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.vo.ProjectTargetAdministrationVO;
import com.hljx.dto.ProjectTargetAdministrationDTO;
import com.hljx.entity.ProjectTargetAdministration;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
public interface ProjectTargetAdministrationService extends IService<ProjectTargetAdministration> {

    IPage<ProjectTargetAdministrationVO> listByCodeOrName(ProjectTargetAdministrationDTO projectTargetAdministrationDTO);


    int insertProject(ProjectTargetAdministration projectTargetAdministration);

    int deleteProject(Long id);
}
