package com.hljx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.bo.vo.AccountingUnitsVO;
import com.hljx.dto.AccountingUnitDTO;
import com.hljx.dto.AccountingUnitPageDTO;
import com.hljx.entity.SysAccountingUnit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 核算单元管理 服务类
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
public interface SysAccountingUnitService extends IService<SysAccountingUnit> {

    /**
     * 保存或编辑核算单元
     * @param accountingUnitDTO
     * @return
     */
    Boolean saveOrUpdate(AccountingUnitDTO accountingUnitDTO);

    /**
     * 分页查询核算单元
     * @param accountingUnitPageDTO
     * @return
     */
    IPage<AccountingUnitsVO> listAccountingUnitByPage(AccountingUnitPageDTO accountingUnitPageDTO);

    /**
     * 删除核算单元
     * @param id
     * @return
     */
    Boolean removeAccountingById(Long id);


}
