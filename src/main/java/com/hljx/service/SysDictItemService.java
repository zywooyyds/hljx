package com.hljx.service;

import com.hljx.entity.SysDictItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典项 服务类
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
public interface SysDictItemService extends IService<SysDictItem> {

}
