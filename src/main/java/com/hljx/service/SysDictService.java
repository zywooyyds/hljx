package com.hljx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.bo.vo.SysDictVO;
import com.hljx.dto.SysDictDTO;
import com.hljx.dto.SysDictPageDTO;
import com.hljx.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 字典 服务类
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
public interface SysDictService extends IService<SysDict> {
    /**
     * 保存或编辑字典
     * @param sysDictDTO
     * @return
     */
    Boolean saveOrUpdateDict(SysDictDTO sysDictDTO);

    /**
     * 删除
     * @param id
     * @return
     */
    Boolean deleteDict(Long id);

    /**
     *
     * 分页查询字典
     * @param sysDictPageDTO
     * @return
     */
    IPage<SysDictVO> listSysDictByPage(SysDictPageDTO sysDictPageDTO);

}
