package com.hljx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.bo.vo.HospitalAreasVO;
import com.hljx.dto.HospitalAreaDTO;
import com.hljx.dto.HospitalAreaPageDTO;
import com.hljx.entity.SysHospitalArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 院区管理 服务类
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */


public interface SysHospitalAreaService extends IService<SysHospitalArea> {
    /**
     * 保存或编辑院区
     * @param hospitalAreaDTO
     * @return
     */
    Boolean saveOrUpdateHospitalArea(HospitalAreaDTO hospitalAreaDTO);

    /**
     * 删除院区
     * @param id
     * @return
     */
    Boolean deleteHospitalArea(Long id);

    /**
     *
     * 分页查询院区
     * @param hospitalAreaPageDTO
     * @return
     */
    IPage<HospitalAreasVO> listHospitalAreaByPage(HospitalAreaPageDTO hospitalAreaPageDTO);

}
