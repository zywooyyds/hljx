package com.hljx.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.hljx.dto.WorkloadTargetAdministrationCopyDTO;
import com.hljx.vo.WorkloadTargetAdministrationDeptProjectVO;
import com.hljx.dto.WorkloadTargetAdministrationDTO;
import com.hljx.dto.WorkloadTargetAdministrationInsertDTO;
import com.hljx.entity.WorkloadTargetAdministration;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 工作量指标管理主表 服务类
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
public interface WorkloadTargetAdministrationService extends IService<WorkloadTargetAdministration> {

    int insertWorkloadTargetAdministration(WorkloadTargetAdministrationInsertDTO insertDTO);

    int updateWorkloadTargetAdministration(WorkloadTargetAdministrationInsertDTO updateDTO);

    IPage<WorkloadTargetAdministrationDeptProjectVO> listByPage(WorkloadTargetAdministrationDTO workloadTargetAdministrationDTO);

    int inheritWorkloadTargetAdministration(WorkloadTargetAdministrationCopyDTO copyDTO);

    int deleteWorkloadAdministration(WorkloadTargetAdministrationDTO workloadTargetAdministrationDTO);
}
