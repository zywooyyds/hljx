package com.hljx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hljx.vo.AssessmentCyclePullVO;
import com.hljx.config.exception.DataException;
import com.hljx.dto.AssessmentCycleDto;
import com.hljx.entity.AssessmentCycle;
import com.hljx.mapper.AssessmentCycleMapper;
import com.hljx.service.AssessmentCycleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.hljx.config.exception.DataEnums.DATA_REPEAT;

/**
 * <p>
 * 考核周期表 服务实现类
 * </p>
 *
 * @author lp
 * @since 2023-09-14
 */
@Service
public class AssessmentCycleServiceImpl extends ServiceImpl<AssessmentCycleMapper, AssessmentCycle> implements AssessmentCycleService {

    @Autowired
    private AssessmentCycleMapper assessmentCycleMapper;


    @Override
    public boolean saveAssessmentCycle(AssessmentCycle assessmentCycle) {
        int i = baseMapper.searchAllByStartDateAndEndDateInt(assessmentCycle);
        if (i>0){
            throw new DataException("日期重复",201);
        }
        Long aLong = baseMapper.selectCount(Wrappers.lambdaQuery(AssessmentCycle.class)
                .eq(AssessmentCycle::getYear, assessmentCycle.getYear())
                .eq(AssessmentCycle::getTimes, assessmentCycle.getTimes())
                .eq(AssessmentCycle::getDeleteFlag,Boolean.FALSE));
        if (aLong>0){
            throw new DataException(DATA_REPEAT);
        }
        return save(assessmentCycle);
    }

    @Override
    public IPage<AssessmentCycle> listAll(AssessmentCycleDto assessmentCycleDto) {
        IPage page = page(assessmentCycleDto.getPage(), Wrappers.lambdaQuery(AssessmentCycle.class).eq(AssessmentCycle::getDeleteFlag, Boolean.FALSE).orderBy(true,true,AssessmentCycle::getCreateTime));
        return page;
    }

    @Override
    public boolean deleteAssessmentCycleById(long id) {
        boolean update = update(Wrappers.lambdaUpdate(AssessmentCycle.class).eq(AssessmentCycle::getId, id).set(AssessmentCycle::getDeleteFlag, Boolean.TRUE));
        return update;
    }

    /**
     * @param
     * @author hxc
     * @date 2023/9/19 17:22
     * @version 1.0.3
     * @see    "工作量指标管理，周期下拉框"
     */
    @Override
    public List<AssessmentCyclePullVO> getAssessmentCyclePull() {
        List<AssessmentCyclePullVO> assessmentCyclePull = assessmentCycleMapper.getAssessmentCyclePull();
        return assessmentCyclePull;
    }
}
