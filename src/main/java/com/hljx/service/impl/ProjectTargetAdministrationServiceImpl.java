package com.hljx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.vo.ProjectTargetAdministrationVO;
import com.hljx.config.exception.DataException;
import com.hljx.dto.ProjectTargetAdministrationDTO;
import com.hljx.entity.ProjectTargetAdministration;
import com.hljx.mapper.ProjectTargetAdministrationMapper;
import com.hljx.service.ProjectTargetAdministrationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Service
public class ProjectTargetAdministrationServiceImpl extends ServiceImpl<ProjectTargetAdministrationMapper, ProjectTargetAdministration> implements ProjectTargetAdministrationService {

    @Autowired
    private ProjectTargetAdministrationMapper projectTargetAdministrationMapper;

    /**
     * @param
     * @author hxc
     * @date 2023/9/18 16:55
     * @version 1.0
     * @see "分页模糊查询"
     */
    @Override
    public IPage<ProjectTargetAdministrationVO> listByCodeOrName(ProjectTargetAdministrationDTO projectTargetAdministrationDTO) {
        Page<ProjectTargetAdministrationVO> page = new Page<>(projectTargetAdministrationDTO.getPageNum(), projectTargetAdministrationDTO.getPageSize());
        IPage<ProjectTargetAdministrationVO> projectTargetAdministrationVOIPage = projectTargetAdministrationMapper.listByCodeOrName(page, projectTargetAdministrationDTO);

        return projectTargetAdministrationVOIPage;
    }

    /**
     * @param
     * @author hxc
     * @date 2023/9/18 16:54
     * @version 1.0
     * @see  "添加项目"
     */
    @Override
    @Transactional
    public int insertProject(ProjectTargetAdministration projectTargetAdministration) {
        //判重
        int distinct = projectTargetAdministrationMapper.distinct(null, projectTargetAdministration.getProjectCode());
        if (distinct>0){
            throw new DataException("项目编码已存在",601);
        }
        int insert = projectTargetAdministrationMapper.insert(projectTargetAdministration);
        return insert;
    }

    /**
     * @param
     * @author hxc
     * @date 2023/9/18 16:54
     * @version 1.0
     * @see  "逻辑删除"
     */
    @Override
    public int deleteProject(Long id) {
        int i = projectTargetAdministrationMapper.updateByidDelete(id);
        return i;
    }
}
