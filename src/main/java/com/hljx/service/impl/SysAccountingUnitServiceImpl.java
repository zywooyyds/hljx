package com.hljx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.bo.vo.AccountingUnitsVO;
import com.hljx.config.exception.DataException;
import com.hljx.dto.AccountingUnitDTO;
import com.hljx.dto.AccountingUnitPageDTO;
import com.hljx.entity.SysAccountingUnit;
import com.hljx.mapper.SysAccountingUnitMapper;
import com.hljx.service.SysAccountingUnitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 核算单元管理 服务实现类
 * </p>
 *
 * @author gx
 * @since 2023-09-18
 */
@Service
public class SysAccountingUnitServiceImpl extends ServiceImpl<SysAccountingUnitMapper, SysAccountingUnit> implements SysAccountingUnitService {

    @Override
    public Boolean saveOrUpdate(AccountingUnitDTO dto) {
        long count = count(Wrappers.lambdaQuery(SysAccountingUnit.class)
                .eq(SysAccountingUnit::getCode, dto.getCode())
                .ne(dto.getId() != null, SysAccountingUnit::getId, dto.getId()));
        if (count > 0) {
            throw new DataException("核算单元编码重复",201);
        }
        SysAccountingUnit sysAccountingUnit = new SysAccountingUnit();
        BeanUtil.copyProperties(dto, sysAccountingUnit);
        return saveOrUpdate(sysAccountingUnit);
    }

    @Override
    public IPage<AccountingUnitsVO> listAccountingUnitByPage(AccountingUnitPageDTO dto) {
        Page<AccountingUnitsVO> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        return baseMapper.listAccountingUnitByPage(page, dto);
    }

    @Override
    public Boolean removeAccountingById(Long id) {
        return removeById(id);
    }
}
