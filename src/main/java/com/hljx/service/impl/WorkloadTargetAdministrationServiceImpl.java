package com.hljx.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hljx.dto.WorkloadTargetAdministrationCopyDTO;
import com.hljx.mapper.WorkloadTargetProjectMapper;
import com.hljx.vo.WorkloadTargetAdministrationDeptProjectVO;
import com.hljx.config.exception.DataException;
import com.hljx.dto.WorkloadTargetAdministrationDTO;
import com.hljx.dto.WorkloadTargetAdministrationInsertDTO;
import com.hljx.entity.WorkloadTargetAdministration;
import com.hljx.entity.WorkloadTargetDept;
import com.hljx.entity.WorkloadTargetProject;
import com.hljx.mapper.WorkloadTargetAdministrationMapper;
import com.hljx.mapper.WorkloadTargetDeptMapper;
import com.hljx.service.WorkloadTargetAdministrationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 工作量指标管理主表 服务实现类
 * </p>
 *
 * @author hxc
 * @since 2023-09-18
 */
@Service
public class WorkloadTargetAdministrationServiceImpl extends ServiceImpl<WorkloadTargetAdministrationMapper, WorkloadTargetAdministration> implements WorkloadTargetAdministrationService {

    @Autowired
    private WorkloadTargetAdministrationMapper workloadTargetAdministrationMapper;

    @Autowired
    private WorkloadTargetDeptServiceImpl workloadTargetDeptInfoService;

    @Autowired
    private WorkloadTargetProjectServiceImpl workloadTargetProjectService;

    @Autowired
    private WorkloadTargetDeptMapper workloadTargetDeptMapper;

    @Autowired
    private WorkloadTargetProjectMapper workloadTargetProjectMapper;


    /**
     * @param
     * @author hxc
     * @date 2023/9/18 17:22
     * @version 1.0
     * @see "添加工作量指标管理"
     */
    @Override
    @Transactional
    public int insertWorkloadTargetAdministration(WorkloadTargetAdministrationInsertDTO insertDTO) {
        //主表实体类
        //判重
        int distinct = workloadTargetAdministrationMapper.distinct(insertDTO.getWorkloadCode());
        if (distinct>0){
            throw new DataException("工作量指标编码已存在",1002);
        }
        //添加主表信息
        int i = workloadTargetAdministrationMapper.insertWorkloadTarget(insertDTO.getPeriod(), insertDTO);
        //获取新增的主键id
        Long aLong = workloadTargetAdministrationMapper.byCodeGetId(insertDTO.getPeriod(), insertDTO.getWorkloadCode());

        //科室实体类
        List<WorkloadTargetDept> dept = insertDTO.getDept();
        for (WorkloadTargetDept deptInfo:dept){
            deptInfo.setWorkloadTargetAdministrationId(aLong);
        }
        //添加科室
        boolean saveBatchDept = workloadTargetDeptInfoService.saveBatch(dept);

        //项目实体类
        List<WorkloadTargetProject> project = insertDTO.getProject();
        for (WorkloadTargetProject projectInfo:project){
            projectInfo.setWorkloadTargetAdministrationId(aLong);
        }
        boolean saveBatchProject = workloadTargetProjectService.saveBatch(project);
        if (!saveBatchDept || !saveBatchProject){
            throw new DataException("科室或项目添加失败",1003);
        }
        return i;
    }

    /**
     * @param
     * @author hxc
     * @date 2023/9/20 15:00
     * @version 1.0
     * @see   "编辑"
     */
    @Override
    @Transactional
    public int updateWorkloadTargetAdministration(WorkloadTargetAdministrationInsertDTO updateDTO) {
        //删除科室信息
        int deleteDept = workloadTargetDeptMapper.ByWorkloadTargetAdministrationIdDeleteDept(updateDTO.getId(), updateDTO.getPeriod());
        //添加科室信息
        List<WorkloadTargetDept> dept = updateDTO.getDept();
        for (WorkloadTargetDept deptInfo:dept){
            deptInfo.setWorkloadTargetAdministrationId(updateDTO.getId());
        }
        //添加科室
        boolean saveBatchDept = workloadTargetDeptInfoService.saveBatch(dept);

        //删除项目信息
        int deleteProject = workloadTargetProjectMapper.ByWorkloadTargetAdministrationIdDeleteProject(updateDTO.getId(), updateDTO.getPeriod());
        //添加新编辑项目信息
        List<WorkloadTargetProject> project = updateDTO.getProject();
        for (WorkloadTargetProject projectInfo:project){
            projectInfo.setWorkloadTargetAdministrationId(updateDTO.getId());
        }
        boolean saveBatchProject = workloadTargetProjectService.saveBatch(project);
        if (deleteDept<0 || !saveBatchDept || deleteProject<0 || !saveBatchProject){
            throw new DataException("编辑失败",1004);
        }
        return deleteDept;
    }

    /**
     * @param
     * @author hxc
     * @date 2023/9/19 11:22
     * @version 1.0
     * @see  "分页模糊查询"
     */
    @Override
    public IPage<WorkloadTargetAdministrationDeptProjectVO> listByPage(WorkloadTargetAdministrationDTO workloadTargetAdministrationDTO) {
        Page<WorkloadTargetAdministrationDeptProjectVO> page = new Page<>(workloadTargetAdministrationDTO.getPageNum(), workloadTargetAdministrationDTO.getPageSize());

        //TODO   返回主键id，编辑要用到

        return null;
    }

    /**
     * @param
     * @author hxc
     * @date 2023/9/20 14:30
     * @version 1.0
     * @see  "继承"
     * 创建时间也会继承过去，如何解决
     */
    @Override
    @Transactional
    public int inheritWorkloadTargetAdministration(WorkloadTargetAdministrationCopyDTO copyDTO) {
        int i = workloadTargetAdministrationMapper.inheritWorkloadTargetAdministration(copyDTO);
        int i1 = workloadTargetDeptMapper.inheritWorkloadTargetDept(copyDTO);
        int i2 = workloadTargetProjectMapper.inheritWorkloadTargetProject(copyDTO);
        if (i<0 || i1<0 || i2<0){
            throw new DataException("继承失败",1004);
        }


        return i2;
    }

    /**
     * @param
     * @author hxc
     * @date 2023/9/20 15:40
     * @version 1.0
     * @see "删除工作量指标管理"
     */
    @Override
    @Transactional
    public int deleteWorkloadAdministration(WorkloadTargetAdministrationDTO dto) {
        //删除工作量指标管理
        int i = workloadTargetAdministrationMapper.deleteByWorkloadTargetAdministrationId(dto.getPeriod(), dto.getId());
        //删除科室信息
        int i1 = workloadTargetDeptMapper.deleteByWorkloadTargetDept(dto.getPeriod(), dto.getId());
        //删除项目信息
        int i2 = workloadTargetProjectMapper.deleteByWorkloadTargetProject(dto.getPeriod(), dto.getId());
        if (i<0 || i1<0 || i2<0){
            throw new DataException("继承失败",1006);
        }
        return i2;
    }
}
