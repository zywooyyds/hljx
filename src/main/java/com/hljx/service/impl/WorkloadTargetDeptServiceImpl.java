package com.hljx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hljx.entity.WorkloadTargetDept;
import com.hljx.mapper.WorkloadTargetDeptMapper;
import com.hljx.service.WorkloadTargetDeptService;
import org.springframework.stereotype.Service;

/**
 * @Author: 何现成
 * @Date: 2023/9/19 10:00
 */
@Service
public class WorkloadTargetDeptServiceImpl extends ServiceImpl<WorkloadTargetDeptMapper, WorkloadTargetDept> implements WorkloadTargetDeptService {

}
