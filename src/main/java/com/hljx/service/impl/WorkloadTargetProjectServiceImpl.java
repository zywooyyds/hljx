package com.hljx.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hljx.entity.WorkloadTargetProject;
import com.hljx.mapper.WorkloadTargetProjectMapper;
import com.hljx.service.WorkloadTargetProjectService;
import org.springframework.stereotype.Service;

/**
 * @Author: 何现成
 * @Date: 2023/9/19 10:16
 */
@Service
public class WorkloadTargetProjectServiceImpl extends ServiceImpl<WorkloadTargetProjectMapper, WorkloadTargetProject> implements WorkloadTargetProjectService {

}
