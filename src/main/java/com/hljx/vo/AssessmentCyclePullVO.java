package com.hljx.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @Author: 何现成
 * @Date: 2023/9/19 17:10
 */
@Data
public class AssessmentCyclePullVO {

    @Schema(description = "年")
    private Integer year;

    @Schema(description = "次数")
    private Integer times;

    @Schema(description = "下拉框")
    private String periodPull;

    @Schema(description = "表格后缀")
    private String periodSuffix;

}
