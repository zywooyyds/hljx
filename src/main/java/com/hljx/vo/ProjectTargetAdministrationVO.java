package com.hljx.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @Author: 何现成
 * @Date: 2023/9/18 15:47
 */
@Data
public class ProjectTargetAdministrationVO {

    /**
     * 统计项目编码
     */
    @Schema(description = "项目编码")
    private String projectNumber;

    /**
     * 统计项目名称
     */
    @Schema(description = "项目名称")
    private String projectName;

    /**
     * 来源
     */
    @Schema(description = "来源")
    private String source;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;
}
