package com.hljx.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: 何现成
 * @Date: 2023/9/19 11:24
 */
@Data
public class WorkloadTargetAdministrationDeptProjectVO {

    @Schema(description = "工作量指标编码")
    private String workloadCode;

    @Schema(description = "工作量指标名称")
    private String workloadName;

    @Schema(description = "科室编码信息")
    private String deptCodeInfo;

    @Schema(description = "科室名称信息")
    protected String deptNameInfo;

    @Schema(description = "统计项目编码")
    private String projectCode;

    @Schema(description = "统计项目名称")
    private String projectName;

    @Schema(description = "点数")
    private BigDecimal count;

    @Schema(description = "单位")
    private String unit;

}
