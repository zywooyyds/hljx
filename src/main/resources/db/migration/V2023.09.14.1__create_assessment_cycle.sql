CREATE TABLE `jx`.`assessment_cycle`  (
                                  `id` bigint NOT NULL COMMENT '主键',
                                  `year` int NOT NULL COMMENT '年',
                                  `times` int NOT NULL COMMENT '次数',
                                  `assessment_cycle_name` varchar(50) NOT NULL COMMENT '考核周期名称',
                                  `start_date` date NOT NULL COMMENT '开始时间',
                                  `end_date` date NOT NULL COMMENT '结束时间',
                                  PRIMARY KEY (`id`)
) COMMENT = '考核周期表';