ALTER TABLE `jx`.`assessment_cycle`
    ADD COLUMN `create_by` varchar(255) NULL COMMENT '创建人' AFTER `end_date`,
ADD COLUMN `create_time` datetime NULL COMMENT '创建时间' AFTER `create_by`,
ADD COLUMN `update_by` datetime NULL COMMENT '更新人' AFTER `create_time`,
ADD COLUMN `update_time` datetime NULL COMMENT '更新时间' AFTER `update_by`,
ADD COLUMN `delete_flag` varchar(255) NULL COMMENT '逻辑删除' AFTER `update_time`;