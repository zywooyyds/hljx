ALTER TABLE `jx`.`assessment_cycle`
    MODIFY COLUMN `delete_flag` bit NULL DEFAULT 0 COMMENT '逻辑删除' AFTER `update_time`;