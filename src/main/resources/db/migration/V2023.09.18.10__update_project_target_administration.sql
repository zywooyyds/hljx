ALTER TABLE `jx`.`project_target_administration`
    CHANGE COLUMN `project_number` `project_code` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '统计项目编码' AFTER `project_name`,
    MODIFY COLUMN `source` bigint(255) NULL DEFAULT NULL COMMENT '来源' AFTER `project_code`,
    MODIFY COLUMN `delete_flag` bit(11) NULL DEFAULT 0 COMMENT '是否删除（0未删除，1已删除）' AFTER `remark`;