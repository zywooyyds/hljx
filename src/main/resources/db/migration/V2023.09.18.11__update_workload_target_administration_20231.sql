ALTER TABLE `jx`.`workload_target_administration_20231`
    CHANGE COLUMN `workload_number` `workload_code` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '工作量指标编码' AFTER `id`,
    MODIFY COLUMN `delete_flag` bit(11) NULL DEFAULT 0 COMMENT '是否删除（0未删除，1已删除）' AFTER `is_file`;