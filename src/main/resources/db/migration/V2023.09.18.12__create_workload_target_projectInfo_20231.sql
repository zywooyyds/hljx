CREATE TABLE `jx`.`workload_target_projectinfo_20231`  (
                                  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `workload_target_administration_id` bigint(11) NOT NULL COMMENT '工作量指标管理主键id',
                                  `target_administration_id` bigint(11) NOT NULL COMMENT '统计项目管理主键id',
                                  `count` double NOT NULL COMMENT '点数',
                                  `unit` bigint(255) NOT NULL COMMENT '单位',
                                  `delete_flag` bit(11) NULL DEFAULT 0 COMMENT '是否删除（0未删除，1已删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL,
                                  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `update_time` datetime(0) NULL DEFAULT NULL,
                                  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) COMMENT = '工作量指标管理项目信息表';