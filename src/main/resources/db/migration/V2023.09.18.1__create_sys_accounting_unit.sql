CREATE TABLE `sys_accounting_unit` (
                                       `id` bigint NOT NULL AUTO_INCREMENT COMMENT '核算单元主键',
                                       `hospital_id` bigint DEFAULT NULL COMMENT '院区id',
                                       `dict_item_id` bigint DEFAULT NULL COMMENT '字典数据项id(类别)',
                                       `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '核算单元名称',
                                       `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '核算单元编码',
                                       `work_weight` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0.0' COMMENT '工作权重',
                                       `delete_flag` int DEFAULT '0' COMMENT '是否删除（0未删除 1已删除）',
                                       `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                                       `create_by` varchar(255) DEFAULT NULL,
                                       `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                                       `update_by` varchar(255) DEFAULT NULL,
                                       PRIMARY KEY (`id`) USING BTREE,
                                       KEY `id` (`id`) USING BTREE,
                                       KEY `hospital_area` (`hospital_id`) USING BTREE,
                                       KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='核算单元管理表';