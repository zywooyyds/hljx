CREATE TABLE `sys_dict` (
                            `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
                            `name` varchar(64) NOT NULL COMMENT '名称',
                            `code` varchar(64) DEFAULT '' COMMENT '编码',
                            `status` int DEFAULT '1' COMMENT '启用状态：0->禁用 1->启用',
                            `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '数据来源(值只能是this_system/his/nursing_management_system/supply_room_system/hand_numbness_system)',
                            `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '备注',
                            `delete_flag` int DEFAULT '0' COMMENT '是否删除（0未删除，1已删除）',
                            `create_time` datetime DEFAULT NULL,
                            `create_by` varchar(255) DEFAULT NULL,
                            `update_time` datetime DEFAULT NULL,
                            `update_by` varchar(255) DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE,
                            KEY `id` (`id`) USING BTREE,
                            KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='字典表';