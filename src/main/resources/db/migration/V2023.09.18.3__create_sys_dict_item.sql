CREATE TABLE `sys_dict_item` (
                                 `id` bigint NOT NULL AUTO_INCREMENT COMMENT '项主键',
                                 `dict_id` bigint DEFAULT NULL COMMENT '字典父id',
                                 `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '字典标签',
                                 `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '字典code',
                                 `sort` int DEFAULT '0' COMMENT '字典排序',
                                 `status` int DEFAULT NULL COMMENT '启用状态：0->禁用 1->启用',
                                 `remark` text COMMENT '备注',
                                 `delete_flag` int DEFAULT '0' COMMENT '是否删除（0未删除，1已删除）',
                                 `create_time` datetime DEFAULT NULL,
                                 `create_by` varchar(255) DEFAULT NULL,
                                 `update_time` datetime DEFAULT NULL,
                                 `update_by` varchar(255) DEFAULT NULL,
                                 PRIMARY KEY (`id`) USING BTREE,
                                 KEY `id` (`id`) USING BTREE,
                                 KEY `dict_id` (`dict_id`) USING BTREE,
                                 KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='字典项表';