CREATE TABLE `sys_hospital_area` (
                                     `id` bigint NOT NULL AUTO_INCREMENT COMMENT '院区主键',
                                     `code` varchar(100) DEFAULT NULL COMMENT '编码',
                                     `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '院区名称',
                                     `remark` text COMMENT '备注',
                                     `delete_flag` int DEFAULT '0' COMMENT '是否删除（0未删除 1已删除）',
                                     `create_time` datetime DEFAULT NULL,
                                     `create_by` varchar(255) DEFAULT NULL,
                                     `update_time` datetime DEFAULT NULL,
                                     `update_by` varchar(255) DEFAULT NULL,
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='院区管理表';