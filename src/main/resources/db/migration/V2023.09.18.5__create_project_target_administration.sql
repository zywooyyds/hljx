CREATE TABLE `jx`.`project_target_administration`  (
                                  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '统计项目名称',
                                  `project_number` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '统计项目编码',
                                  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '来源',
                                  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
                                  `delete_flag` int(11) NULL DEFAULT 0 COMMENT '是否删除（0未删除，1已删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL,
                                  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `update_time` datetime(0) NULL DEFAULT NULL,
                                  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) COMMENT = '统计项目管理表';