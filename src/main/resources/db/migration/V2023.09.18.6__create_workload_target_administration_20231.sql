CREATE TABLE `jx`.`workload_target_administration_20231`  (
                                  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
                                  `workload_number` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '工作量指标编码',
                                  `workload_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '工作量指标名称',
                                  `is_file` bit(1) NULL DEFAULT 1 COMMENT '0是已归档/1是未归档',
                                  `delete_flag` int(11) NULL DEFAULT 0 COMMENT '是否删除（0未删除，1已删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL,
                                  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `update_time` datetime(0) NULL DEFAULT NULL,
                                  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) COMMENT = '工作量指标管理主表';