CREATE TABLE `jx`.`workload_target_deptinfo_20231`  (
                                  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                  `dept_info` varchar(255) NULL COMMENT '科室信息',
                                  `workload_target_administration_id` int(0) NULL COMMENT '工作量指标主键id',
                                  `delete_flag` int(11) NULL DEFAULT 0 COMMENT '是否删除（0未删除，1已删除）',
                                  `create_time` datetime(0) NULL DEFAULT NULL,
                                  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  `update_time` datetime(0) NULL DEFAULT NULL,
                                  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) COMMENT = '工作量指标管理科室信息表';