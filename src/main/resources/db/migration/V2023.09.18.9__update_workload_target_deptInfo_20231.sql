ALTER TABLE `jx`.`workload_target_deptinfo_20231`
    CHANGE COLUMN `dept_info` `dept_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '科室信息' AFTER `id`,
    MODIFY COLUMN `workload_target_administration_id` bigint(11) NULL DEFAULT NULL COMMENT '工作量指标主键id' AFTER `dept_code`,
    MODIFY COLUMN `delete_flag` bit(11) NULL DEFAULT 0 COMMENT '是否删除（0未删除，1已删除）' AFTER `workload_target_administration_id`;